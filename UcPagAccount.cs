﻿using System;

namespace UcPagAccountNS
{
  
    public class Pagamento
    {
        public static double  GerarTaxaJuro()
        {
            return 0.01;
        }
        public  double CalculaJuros(double valorincial, int periodo)
        {
            return valorincial * Math.Pow(1 + GerarTaxaJuro(), (double)periodo);
        }

    }
        
}